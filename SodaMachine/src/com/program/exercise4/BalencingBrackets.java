package com.program.exercise4;

import java.util.Stack;

public class BalencingBrackets {
	public static void main(String[] args) {
		String str = "[{}]";
		System.out.println(hasBalancedBrackets(str));
	}

	public static int hasBalancedBrackets(String str) {
		Stack<Character> brackets = new Stack<>();
		brackets.push('$');
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == ']' || str.charAt(i) == '[' || str.charAt(i) == '}' || str.charAt(i) == '{'
					|| str.charAt(i) == ')' || str.charAt(i) == '(' || str.charAt(i) == '>' || str.charAt(i) == '<')
			{
				if ((str.charAt(i) == ']' && brackets.peek() == '[') || (str.charAt(i) == '}' && brackets.peek() == '{')
						|| (str.charAt(i) == ')' && brackets.peek() == '(')
						|| (str.charAt(i) == '>' && brackets.peek() == '<')) {
					brackets.pop();
				} else {
					brackets.push(str.charAt(i));
				}
			}
			else
				return 0;

		}
		if (brackets.peek() == '$')
			return 1;
		else
			return 0;
	}
}
