package com.program.exercise4;

import java.util.ArrayList;
import java.util.List;

public class TestLab {

	public static void main(String[] args) {

		List<Student> students = new ArrayList<Student>();
		DrinksMachine drinksMachine = new DrinksMachine(10);
		TrashCan bin = new TrashCan(4);

		LabWorker labWorker = new LabWorker();
		labWorker.setMachine(drinksMachine);
		labWorker.setBin(bin);

		students.add(labWorker);

		Student student1 = new Student("John", 3);
		student1.setMachine(drinksMachine);
		student1.setBin(bin);
		Student student2 = new Student("Malorie", 2);
		student2.setMachine(drinksMachine);
		student2.setBin(bin);
		Student student3 = new Student("Scott", 5);
		student3.setMachine(drinksMachine);
		student3.setBin(bin);

		students.add(student1);
		students.add(student2);
		students.add(student3);

		for (Student student : students) {
			student.doAction();
			student.doAction();
			student.doAction();
			System.out.println();
			System.out.println(bin.displaySelf());
			System.out.println();
			labWorker.doAction();
			System.out.println();

		}

	}
}
