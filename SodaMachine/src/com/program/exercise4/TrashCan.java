package com.program.exercise4;



public class TrashCan {

	private SodaCan[] sodaCans;
	private int nextFree;

	public TrashCan(int size) {
		this.sodaCans = new SodaCan[size];
	}

	public void addCan(SodaCan can) {
		this.sodaCans[this.nextFree++] = can;
	}

	public String displaySelf() {
		if (this.sodaCans.length == 0)
			return "Bin is empty";
		else if (this.sodaCans.length == this.nextFree)
			return "Bin is full";
		else
			return "Bin has " + this.sodaCans.length + " cans";

	}

	public boolean isFull() {
		return this.sodaCans.length == this.nextFree;
	}

	public void emptyBin() {
		for (int i = 0; i < this.sodaCans.length; i++) {
			this.sodaCans[i] = null;
		}
		this.nextFree=0;
	}
}
