package com.program.exercise4;

class Node {
	Node left;
	Node right;
	int data;

	public Node(int data, Node left, Node right) {
		this.left = left;
		this.right = right;
		this.data = data;
	}
}

public class BinarySearchTree {
	static Node root;

	private static void insertANode(int value) {
		Node newNode = new Node(value, null, null);
		if (root == null) {
			root = newNode;
			root.left = null;
			root.right = null;
			return;
		}
		Node current = root;
		while (true) {
			Node parent = current;

			if (value < current.data) {
				current = current.left;
				if (current == null) {
					parent.left = newNode;
					return;
				}
			} else {
				current = current.right;
				if (current == null) {
					parent.right = newNode;
					return;
				}
			}
		}

	}

	private static int calculateDistance(Node root, int n1, int n2) {
		int x = pathDistance(root, n1) - 1;
		int y = pathDistance(root, n2) - 1;
		int data = findLCA(root, n1, n2).data;
		int distance = pathDistance(root, data) - 1;
		return (x + y) - 2 * distance;

	}

	private static int pathDistance(Node root, int n1) {
		if (root != null) {
			int count = 0;
			if ((root.data == n1) || (count = pathDistance(root.left, n1)) > 0
					|| (count = pathDistance(root.right, n1)) > 0) {
				return count + 1;
			}
			return 0;

		}
		return 0;
	}

	private static Node findLCA(Node root, int n1, int n2) {
		if (root != null) {
			if (root.data == n1 || root.data == n2)
				return root;

			Node left = findLCA(root.left, n1, n2);
			Node right = findLCA(root.right, n1, n2);
			if (left != null && right != null)
				return root;
			if (left != null)
				return left;
			if (right != null)
				return right;
		}
		return null;
	}

	public static int bstDistance(int[] values, int n, int node1, int node2) {
		if(n<=0)
			return -1;
		boolean n1Flag = false, n2Flag = false;
		for (int i = 0; i < n; i++) {
			if(values[i]<0)
				return -1;
			insertANode(values[i]);
			if(values[i]>Math.pow(2, 32))
				return -1;
		
			if (values[i] == node1)
				n1Flag = true;
			if (values[i] == node2)
				n2Flag = true;
		}
		if (!n1Flag)
			return -1;
		if (!n2Flag)
			return -1;
		return calculateDistance(root, node1, node2);
	}

	public static void main(String[] args) {

		int a[] = { 5, 6, 3, 1, 2, 4 };
		int x = bstDistance(a, 6, 2, 4);
		System.out.println(x);

	}

}
