package com.program.exercise4;

import java.util.ArrayList;
import java.util.List;

public class DrinksMachine {

	private List<SodaCan> sodaCans;
	private SodaCan slot;
	@SuppressWarnings("unused")
	private List<Token> tokens;

	public DrinksMachine(int count) {
		sodaCans = new ArrayList<SodaCan>();
		tokens = new ArrayList<Token>();
		for (int i = 0; i < count; i++) {
			sodaCans.add(new SodaCan(SodaType.coke));
			sodaCans.add(new SodaCan(SodaType.pepsi));
			sodaCans.add(new SodaCan(SodaType.DrPeper));
			sodaCans.add(new SodaCan(SodaType.Dew));
		}

	}

	public void insertCoin(Token coin, SodaType type) {
		for (int i = 0; i < sodaCans.size(); i++) {
			if (sodaCans.get(i).getType().equals(type)) {
				slot = sodaCans.remove(i);
				break;
			}
		}
	}

	public SodaCan deliverCan() {
		SodaCan deliveryCan = slot;
		slot = null;
		return deliveryCan;
	}

	public boolean isEmpty(SodaType type) {
		boolean isAvailable = false;
		if (sodaCans.size() != 0) {
			for (int i = 0; i < sodaCans.size(); i++) {
				if (sodaCans.get(i).getType().equals(type)) {
					isAvailable = true;
					break;
				}
			}
		}
		return !isAvailable;
	}

	public String displaySelf() {
		int coke = 0, pepsi = 0, DrPeper = 0, Dew = 0;

		for (int i = 0; i < sodaCans.size(); i++) {
			if (sodaCans.get(i).getType().equals(SodaType.coke)) {
				coke++;
			}
			if (sodaCans.get(i).getType().equals(SodaType.pepsi)) {
				pepsi++;
			}
			if (sodaCans.get(i).getType().equals(SodaType.DrPeper)) {
				DrPeper++;
			}
			if (sodaCans.get(i).getType().equals(SodaType.Dew)) {
				Dew++;
			}
		}
		return "The machine contains " + coke + " coke " + pepsi + " pepsi " + DrPeper + " DrPeper " + Dew + " Dew";
	}
}
