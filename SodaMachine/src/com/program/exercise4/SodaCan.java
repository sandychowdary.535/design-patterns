package com.program.exercise4;

public class SodaCan {

	private SodaType sodaType;

	public SodaCan(SodaType sodaType) {
		this.sodaType = sodaType;
	}

	public SodaType getType() {
		return this.sodaType;
	}
}
