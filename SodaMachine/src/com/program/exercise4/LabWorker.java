package com.program.exercise4;

public class LabWorker extends Student {
	public LabWorker() {
		super("Lab Assistant", 0);
	}

	public void addToken() {
		this.purse.add(new Token());
	}

	@Override
	public void doAction() {
		if (this.bin.isFull()) {
			this.bin.emptyBin();
			addToken();
			System.out.println(this.name + " Clears the bin");
			super.doAction();
		}
		else
		{
			System.out.println("Bin is not full");
		}
	}
}
