package com.program.exercise4;

import java.util.ArrayList;
import java.util.List;

public class Student {
	String name;
	List<Token> purse;
	boolean sobbing;
	DrinksMachine machine;
	TrashCan bin;
	SodaCan soda;

	public Student() {
		// TODO Auto-generated constructor stub
	}

	public Student(String name, int nTokens) {
		this.name = name;
		this.purse = new ArrayList<Token>();
		for (int i = 0; i < nTokens; i++) {
			this.purse.add(new Token());
		}
		this.sobbing = false;
		this.machine = null;
		this.bin = null;
		this.soda = null;
	}

	public void setMachine(DrinksMachine aMachine) {
		this.machine = aMachine;
	}

	public void setBin(TrashCan aBin) {
		this.bin = aBin;
	}

	public String displaySelf() {
		if (this.soda != null)
			return "Student have CAN";
		if (this.sobbing)
			return "sobbing";
		else
			return "Student have " + this.purse.size() + " coins";
	}

	public void doAction() {
		if (this.soda != null) {
			if (!this.bin.isFull()) {
				this.bin.addCan(this.soda);
				this.soda = null;
				System.out.println(this.name + " takes a sip");
			}
		} else if (!this.purse.isEmpty() && !machine.isEmpty(SodaType.coke)) {
			this.machine.insertCoin(this.purse.remove(this.purse.size() - 1), SodaType.coke);
			System.out.println(this.name + " inserts a token into the Coke machine");
			this.soda = this.machine.deliverCan();
			if (!this.bin.isFull()) {
				this.bin.addCan(this.soda);
				this.soda = null;
				System.out.println(this.name + " takes a sip");
			}
		} else {
			this.sobbing = true;
			System.out.println(this.name + " is sobbing");
		}
	}

}
